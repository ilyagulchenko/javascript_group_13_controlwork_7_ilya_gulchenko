import {Component} from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent {
  iconFood = 'https://cdn.onlinewebfonts.com/svg/img_174839.png';
  iconDrink = 'https://icon-library.com/images/coffe-cup-icon/coffe-cup-icon-26.jpg';
  iconDelete = 'https://findicons.com/files/icons/1262/amora/256/delete.png';

  hamburgerCount = 0;
  cheeseburgerCount = 0;
  friesCount = 0;
  coffeeCount = 0;
  teaCount = 0;
  colaCount = 0;

  changeHamburgerCount() {
    this.hamburgerCount += 1;
  }

  changeCheeseburgerCount() {
    this.cheeseburgerCount += 1;
  }

  changeFriesCount() {
    this.friesCount += 1;
  }

  changeCoffeeCount() {
    this.coffeeCount += 1;
  }

  changeTeaCount() {
    this.teaCount += 1;
  }

  changeColaCount() {
    this.colaCount += 1;
  }

  getTotalHamburgerSum() {
    return 80 * this.hamburgerCount;
  }

  getTotalCheeseburgerSum() {
    return 90 * this.cheeseburgerCount;
  }

  getTotalFriesSum() {
    return 45 * this.friesCount;
  }

  getTotalCoffeeSum() {
    return 70 * this.coffeeCount;
  }

  getTotalTeaSum() {
    return 50 * this.teaCount;
  }

  getTotalColaSum() {
    return 40 * this.colaCount;
  }

  deleteHamburger() {
    this.hamburgerCount = 0;
  }

  deleteCheeseburger() {
    this.cheeseburgerCount = 0;
  }

  deleteFries() {
    this.friesCount = 0;
  }

  deleteCoffee() {
    this.coffeeCount = 0;
  }

  deleteTea() {
    this.teaCount = 0;
  }

  deleteCola() {
    this.colaCount = 0;
  }
}
